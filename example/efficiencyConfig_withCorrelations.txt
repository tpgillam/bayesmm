# Lines starting with # are comments
# Empty lines are ignored
# Extra whitespace padding is ignored

# This section, if it appears, will be used to determine what file format to
# expect. If it does not appear then "BASIC" is assumed.

# This version is now "WITH_CORRELATIONS", as the file format additionally
# supports the CORRELATION_GROUP construct, as demonstrated below.
VERSION
WITH_CORRELATIONS

# Each line has realEffIndex, fakeEffIndex for categories 0,...,N-1, exactly as
# in the case without correlations
CATEGORIES
0 0
1 1

# Each line has the realEff,uRealEf
# for efficiencies 0,...,nr-1.
REAL

# This indicates that all efficiencies that follow are assumed to have a common
# underlying source of error.

CORRELATION_GROUP
# Each line represents realEff, u_uncorr(realEff), u_corr(realEff)
# u_uncorr is the uncorrelated component of uncertainty, independent for each
# efficiency
# u_corr is the uncertainty on each efficiency from the shared underlying
# source.
0.8  0.01 0.01
0.9  0.05 0.05

# A new 'CORRELATION_GROUP' would automatically end the current group, but it
# can also be finished explicitly if some uncorrelated quantities need to
# be inserted
END_CORRELATION_GROUP

# This line would represent a third real efficiency (index 2) which doesn't have
# a correlated source of uncertainty.
0.9  0.1

# There can be multiple correlation groups within each REAL and FAKE
# but groups can't be spread between REAL and FAKE


# No correlation group is specified here, which specifies that the uncertainties
# are uncorrelated everywhere
FAKE
0.1 0.01
0.2 0.1
