Bayesian fake estimation
============================
Implementation of the Bayesian sampling method for estimating the contribution
of fake events in signal regions. Also provides an implementation of the matrix
method.


Dependencies
------------
The code depends on Boost >= 1.47, GLPK (GNU Linear Programming Kit), and
a C++11 compliant compiler. Boost is required to be installed on your system,
however the latest version of GLPK latter will automatically be retrieved and
built for you. On an SLC6 system you will need newer versions of Boost and GCC.

For ATLAS users, you can use 'ATLAS Local Root Base' to put the software you
need in your path. Namely, running:

```bash
localSetupROOT
localSetupBoost boost-1.55.0-python2.7-x86_64-slc6-gcc48
```
should give you sufficient versions of GCC and Boost respectively (note that the
default for localSetupBoost is not sensible as of 03/03/2015). Make sure you
do this before running any of the installation or build commands below.


Installation
------------
Clone the git repository:

```bash
git clone https://tpgillam@bitbucket.org/tpgillam/bayesmm.git bayesmm
cd bayesmm
```

To build the code, first create a build directory and then run CMake and make.
This will automatically build GLPK 4.55 too. On a straightforward system with
just one compiler, then do:

```bash
mkdir build; cd build
cmake ..
make
```

However if running on an ATLAS/CERN system on which you have just placed a new
gcc in your path, you will likely need to explicitly tell CMake which compiler
you want it to use:

```bash
mkdir build; cd build
CC=gcc CXX=g++ cmake ..
make
```

Note that if you initialise the build directory with the wrong compiler, CMake
caches this -- delete the build directory and start again!

You should now have a binary `sampler` in the build directory, which when run
without arguments should give some help text.


Usage
-----
Running the binary without arguments gives some text that describes the usage. A
simple test run can be performed using the files within the example directory.
If your terminal is still in the build directory from the process above, you can
run:

```bash
./sampler 0 ../example/efficiencyConfig.txt < ../example/observed_events.txt
```

to run the normal Matrix Method on the given event file, with the specified
efficiencies. To run the Gibbs sampler of the Bayesian posterior (ignoring event
catgories where there are no observed events for the sake of speed), instead run
```bash
./sampler 2 ../example/efficiencyConfig.txt 10 < ../example/observed_events.txt
```
which will give 10 samples from the posterior distribution of the fake and tight
rate. If piping through a histogramming utility like
[hist](http://www.hep.phy.cam.ac.uk/~lester/hist/) then one can generate samples
ad infinitum by using `-1` as the third argument above, instead of `10`.

One will usually wish to extract summarised information of the set of drawn
samples, such as a measure of average value and uncertainty. Since the
distributions are typically asymmetric the author has found a median and 68%
credible interval surrounding that median to be a suitable choice of "central
value and asymmetric uncertainty", however naturally other options are also
available. The median is trivial to compute, and can be found using the included
`fitter.py` script, for example
```bash
./sampler 2 ../example/efficiencyConfig.txt 500 < ../example/observed_events.txt | ../python/fitter.py
```
If one wishes to generate a pretty plot, and you have matplotlib installed, you
can use the `fitter_withplot.py` script.  The number of samples should be
increased until the desired precision in the output distribution is obtained.

The final parameter accepted by `sampler` controls how many Gibbs steps are
performed before producing a point. Increasing this naturally slows down the
computation, however since it reflects a "burn-in" period it should be increased
until the final answer is observed not to change. The default value of 100 is
probably conservative.


Observed events file
--------------------
Each line of the `observed_events.txt` file corresponds to:
```
<tight/loose 1>  <tight/loose 2>    <category 1>  <category 2>
```

where "1" and "2" refer to the first and second lepton in the event
respectively (e.g. ordered by pT). This is for the case of an analysis selecting
baseline events with exactly two baseline (loose or tight) leptons, and then
defining the signal region by additionally requiring both of those leptons to
pass the tight requirements.

The file can have any *constant* positive number of leptons in each event.
Support for a selection which allows variable numbers of baseline leptons is
possible but not currently implemented. For a set of events with 3 baseline
leptons each line would represent:
```
<t/l 1> <t/l 2> <t/l 3>   <cat 1> <cat 2> <cat 3>
```


Efficiency configuration file
-----------------------------
`efficiencyConfig.txt` is a file that encodes the efficiencies for each category,
see the comments inside that file for a description of the syntax. There are
other files in the `example` directory which additionally show how to:
* Use correlated uncertainties
* Make several lepton categories use the same real of fake efficiency --
necessary when the binning of real and fake efficiencies is not the same.


Method documentation
--------------------
An extract from my thesis detailing the mathematics & algorithm for both the
Matrix Method and Gibbs samplers is included in the `docs` directory.



Tom Gillam, updated March 2015
