#!/usr/bin/env python3.4

"""
Given a stream of numbers on stdin representing draws from a fake & tight rate,
fit a sensible (Gamma) distribution to it and present the results graphically
using matplotlib.

First and only argument is the name of a pdf file to save a fit plot into
(if none is given the file will not be saved), and additionally on stdout will
appear:

    median,  1 sigma 'down',  1 sigma 'up'

Uncertainties are actual uncertainties, not "median + uncertainty", 
if that makes sense...
"""

import fileinput
import numpy as np
import scipy.stats
import matplotlib.pyplot as plt
import math
import argparse
import sys


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('fit_file', nargs='?', default=None, help='File to save fit plot in')
    parser.add_argument('--fit-mode', default='gamma',
            help='Type of fit to do. Options are "all", "gamma", "rawmedian"')
    return parser.parse_args()


def main():
    options = parse_arguments()

    values = []
    max_value = 0.0
    for line in fileinput.input('-'):
        value = float(line)
        values.append(value)
        max_value = max(max_value, value)

    params_norm = scipy.stats.norm.fit(values)

    # How wide our uncertainty interval should be
    alpha = 0.68

    # Gamma fit
    params_gamma = scipy.stats.gamma.fit(values)
    mean_gamma = scipy.stats.gamma.mean(*params_gamma)
    median_gamma = scipy.stats.gamma.median(*params_gamma)
    one_sigma_gamma = scipy.stats.gamma.interval(alpha, *params_gamma)

    # Compute median directly from data
    values.sort()
    i_median = int(math.floor(len(values) / 2))
    i_down = i_median - int(math.ceil(alpha*len(values) / 2))
    i_up = i_median + int(math.ceil(alpha*len(values) / 2))
    median = values[i_median]
    down = values[i_down]
    up = values[i_up]

    if options.fit_mode == 'all':
        print('{0} {1} {2}'.format(median_gamma, median_gamma - one_sigma_gamma[0], one_sigma_gamma[1] - median_gamma))
        print('{0} {1} {2}'.format(median, median - down, up - median))
    elif options.fit_mode == 'gamma':
        print('{0} {1} {2}'.format(median_gamma, median_gamma - one_sigma_gamma[0], one_sigma_gamma[1] - median_gamma))
    elif options.fit_mode == 'rawmedian':
        print('{0} {1} {2}'.format(median, median - down, up - median))
    else:
        raise RuntimeError('Unknown fit mode: {}'.format(options.fit_mode))
    

    if options.fit_file is None:
        sys.exit()

    x = np.linspace(0, max_value, 200)
    y_norm = scipy.stats.norm.pdf(x, *params_norm)
    y_gamma = scipy.stats.gamma.pdf(x, *params_gamma)
    plt.hist(values, bins=25, normed=1, color='green', 
            histtype='stepfilled', zorder=1, label='Samples')
    plt.plot(x, y_gamma, color='red', lw=3, zorder=3, label='Gamma fit')
    plt.plot((median_gamma, median_gamma), (0, scipy.stats.gamma.pdf(median_gamma, *params_gamma)), 
            color='red', lw=3, ls='dotted', zorder=3, label='Median')

    plt.plot((median, median), (0, scipy.stats.gamma.pdf(median_gamma, *params_gamma)), 
            color='cyan', lw=3, ls='dashed', zorder=3, label='Raw median')
    plt.fill_between(x, y_gamma, 
            where=np.logical_and(x > down, x < up), 
            color='cyan', alpha=.5, zorder=2)

    plt.legend()
    plt.xlabel('Fake & tight rate')
    plt.ylabel('Posterior PDF')
    # plt.show()
    plt.savefig(options.fit_file, bbox_inches='tight')



if __name__ == '__main__':
    main()


