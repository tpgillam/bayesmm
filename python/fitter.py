#!/usr/bin/env python

"""
Given a stream of numbers on stdin representing draws from a fake & tight rate,
fit a sensible (Gamma) distribution to it and present the results.

First and only argument is the name of a pdf file to save a fit plot into
(if none is given the file will not be saved), and additionally on stdout will
appear:

    median,  1 sigma 'down',  1 sigma 'up'

Uncertainties are actual uncertainties, not "median + uncertainty", 
"""

import fileinput
import math
import sys


def main():
    values = []
    for line in fileinput.input('-'):
        value = float(line)
        values.append(value)

    # How wide our uncertainty interval should be
    alpha = 0.68

    # Compute median from data
    values.sort()
    i_median = int(math.floor(len(values) / 2))
    i_down = i_median - int(math.ceil(alpha*len(values) / 2))
    i_up = i_median + int(math.ceil(alpha*len(values) / 2))
    median = values[i_median]
    down = values[i_down]
    up = values[i_up]
    print('{0} {1} {2}'.format(median, median - down, up - median))


if __name__ == '__main__':
    main()


