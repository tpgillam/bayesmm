/**
 * @file MatrixMethod.cxx
 * @author Thomas Gillam <thomas.gillam@cern.ch>
 * @date May, 2013
 * @brief Estimate fake components for events for arbitrary number of leptons
 **/ 

#include "MatrixMethod.h"

#include <algorithm>
#include <cmath>
#include <iostream>

namespace GeneralisedMatrixMethod {


  MatrixMethod::MatrixMethod(bool computeUncertainty, int nLeptons) :  m_computeUncertainty(computeUncertainty), m_presetNumLeptons(true), numLeptons(nLeptons)
  {
    allocateMemory();
  }

  MatrixMethod::~MatrixMethod()
  {
    if (m_presetNumLeptons) {
      clearMemory();
    }
  }


  std::vector<MatrixMethod::Result> MatrixMethod::weightsForInput(const std::vector<Lepton> &leptons)
  {
    this->leptons = &leptons;
    setNumberOfLeptons();

    if (!haveEnoughLeptons()) {
      return std::vector<Result>();
    }

    if (!m_presetNumLeptons) {
      allocateMemory();
    }
    fillLeptonInformation();

    std::vector<Result> results;

    zeroConfiguration(configTLOut);
    while (!isLastConfiguration(configTLOut)) {
      if (!isWantedTLOutConfiguration()) {
        incrementConfiguration(configTLOut);
        continue;
      }

      results.push_back(resultForCurrentTLOutConfiguration());

      incrementConfiguration(configTLOut);
    }

    if (!m_presetNumLeptons) {
      clearMemory();
    }
    
    return results;
  }

  void MatrixMethod::setNumberOfLeptons()
  {
    numLeptons = leptons->size();

    // Truncate the problem to save runtime
    if (numLeptons > 12) numLeptons = 12;
  }

  bool MatrixMethod::haveEnoughLeptons() const
  {
    return (numLeptons >= 1);
  }

  void MatrixMethod::allocateMemory()
  {
    isLepElectron = new bool [numLeptons];
    effReal = new float [numLeptons];
    effFake = new float [numLeptons];
    uEffReal = new float [numLeptons];
    uEffFakeStat = new float [numLeptons];
    uEffFakeUncorr = new float [numLeptons];
    uEffFakeCorr = new float [numLeptons];
    uEffFakeBinIndex = new unsigned int [numLeptons];
    configTLIn = new char [numLeptons];
    configTLOut = new char [numLeptons];
    configRF = new char [numLeptons];
    derivativesReal = new float [numLeptons];
    derivativesFake = new float [numLeptons];
  }

  void MatrixMethod::fillLeptonInformation()
  {
    for (unsigned int i = 0; i < numLeptons; ++i) {
      unsigned int iR = (*leptons)[i].effRealIndex;
      unsigned int iF = (*leptons)[i].effFakeIndex;
      bool isTight = (*leptons)[i].isTight;

      effReal[i] = meansR[iR];
      effFake[i] = meansF[iF];

      if (m_computeUncertainty) {
        uEffReal[i] = uncsR[iR];
        uEffFakeStat[i] = uncsF[iF];
        uEffFakeUncorr[i] = 0.f;
        uEffFakeCorr[i] = 0.f;
        uEffFakeBinIndex[i] = iF;
      }

      if (isTight) configTLIn[i] = 1;
      else configTLIn[i] = 0;
    }
  }

  void MatrixMethod::zeroConfiguration(char *config)
  {
    std::fill(config, config+numLeptons, 0);
  }

  MatrixMethod::Result MatrixMethod::resultForCurrentTLOutConfiguration()
  {
    float weight = 0.f;

    zeroConfiguration(configRF);
    if (m_computeUncertainty) {
      zeroFloatArray(derivativesReal);
      zeroFloatArray(derivativesFake);
    }
    while (!isLastConfiguration(configRF)) {
      if (!isWantedRFConfiguration()) {
        incrementConfiguration(configRF);
        continue;
      }

      float matrixElement = getMatrixElement();
      float inverseFactor = getInverseFactor();

      float thisWeight = matrixElement * inverseFactor;
      weight += thisWeight;
      if (m_computeUncertainty) {
        incrementDerivatives(thisWeight);
      }

      incrementConfiguration(configRF);
    }

    Result result(numLeptons, configTLOut, weight);

    if (m_computeUncertainty) {
      addVarianceInformation(result);
    }
    return result;
  }

  void MatrixMethod::zeroFloatArray(float *array)
  {
    std::fill(array, array+numLeptons, 0.f);
  }

  bool MatrixMethod::isLastConfiguration(const char *config) const
  {
    for (unsigned int i = 0; i < numLeptons; ++i ) {
      if (config[i] > 1) return true;
    }
    return false;
  }

  void MatrixMethod::incrementConfiguration(char *config)
  {
    for (unsigned int i = 0; i < numLeptons; ++i ) {
      if (config[i] == 0) {
        config[i] = 1;
        return;
      } else {
        config[i] = 0;
      }
    }
    
    // Flag as finished
    if (numLeptons > 0) config[0] = 2;
  }

  bool MatrixMethod::isWantedTLOutConfiguration() const
  {
    unsigned int nTight = numTightOrRealInConfig(configTLOut);
    // return (nTight > 1);
    return (nTight == numLeptons);
  }

  bool MatrixMethod::isWantedRFConfiguration() const
  {
    //// TODO refactor
    //bool requireFirstLeptonReal = false;
    //if (numLeptons > 2) requireFirstLeptonReal = true;
    //if (requireFirstLeptonReal && !configRF[0]) return false;

    unsigned int nTightOutputLeptons = numTightOrRealInConfig(configTLOut);
    unsigned int nReal = numTightOrRealInConfig(configRF);
    return (nReal < nTightOutputLeptons);
    //return (!areConfigsEquivalent(configRF, configTLOut, numLeptons));
  }

  bool MatrixMethod::areConfigsEquivalent(const char *config1, const char *config2) const
  {
    for (unsigned int i = 0; i < numLeptons; ++i) {
      if (config1[i] != config2[i]) return false;
    }
    return true;
  }

  unsigned int MatrixMethod::numTightOrRealInConfig(const char *config) const
  {
    unsigned int nTight = 0;
    for (unsigned int i = 0; i < numLeptons; ++i ) {
      if (config[i] == 1) ++nTight;
    }
    return nTight;
  }

  float MatrixMethod::getMatrixElement() const
  {
    float element = 1.f;
    for (unsigned int i = 0; i < numLeptons; ++i ) {
      if (configTLOut[i] == 1 && configRF[i] == 1) {
        element *= effReal[i];
      }
      else if (configTLOut[i] == 1 && configRF[i] == 0) {
        element *= effFake[i];
      }
      else if (configTLOut[i] == 0 && configRF[i] == 1) {
        element *= 1.f - effReal[i];
      }
      else if (configTLOut[i] == 0 && configRF[i] == 0) {
        element *= 1.f - effFake[i];
      }
      else {
        std::cerr << "Error: unknown TLout, RF config" << std::endl;
        std::cerr << configToStr(configTLOut) << " " << configToStr(configRF) << std::endl;
        throw;
      }
    }
    return element;
  }

  float MatrixMethod::getInverseFactor() const
  {
    float element = 1.f;
    for (unsigned int i = 0; i < numLeptons; ++i ) {
      // Pick out cofactor
      if (configTLIn[i] == 1 && configRF[i] == 1) {
        element *= 1.f - effFake[i];
      }
      else if (configTLIn[i] == 0 && configRF[i] == 1) {
        element *= - effFake[i];
      }
      else if (configTLIn[i] == 1 && configRF[i] == 0) {
        element *= - (1.f - effReal[i]);
      }
      else if (configTLIn[i] == 0 && configRF[i] == 0) {
        element *= effReal[i];
      }
      else {
        std::cerr << "Error: unknown RF, TLin config" << std::endl;
        std::cerr << configToStr(configRF) << " " << configToStr(configTLIn) << std::endl;
        throw;
      }
      
      // Divide by determinant
      element /= effReal[i] - effFake[i];
    }
    return element;
  }

  void MatrixMethod::incrementDerivatives(float weight)
  {
    for (unsigned int i = 0; i < numLeptons; ++i) {
      float dTotaldReal = weight * (dLogPhidReal(i) + dLogInvPhidReal(i));
      float dTotaldFake = weight * (dLogPhidFake(i) + dLogInvPhidFake(i));
      
      derivativesReal[i] += dTotaldReal;
      derivativesFake[i] += dTotaldFake;
    }
  }

  float MatrixMethod::dLogPhidReal(unsigned int i) const
  {
    if (configTLOut[i] == 1 && configRF[i] == 1) {
      if (effReal[i] == 0.f) return 0.f;
      return (1.f / effReal[i]);
    }
    else if (configTLOut[i] == 1 && configRF[i] == 0) {
      return 0.f;
    }
    else if (configTLOut[i] == 0 && configRF[i] == 1) {
      if (effReal[i] == 1.f) return 0.f;
      return (1.f / (effReal[i] - 1.f));
    }
    else if (configTLOut[i] == 0 && configRF[i] == 0) {
      return 0.f;
    }
    else {
      std::cerr << "Error: unknown TLout, RF config" << std::endl;
      std::cerr << configToStr(configTLOut) << " " << configToStr(configRF) << std::endl;
      throw;
    }
  }

  float MatrixMethod::dLogPhidFake(unsigned int i) const
  {
    if (configTLOut[i] == 1 && configRF[i] == 1) {
      return 0.f;
    }
    else if (configTLOut[i] == 1 && configRF[i] == 0) {
      if (effFake[i] == 0.f) return 0.f;
      return (1.f / effFake[i]);
    }
    else if (configTLOut[i] == 0 && configRF[i] == 1) {
      return 0.f;
    }
    else if (configTLOut[i] == 0 && configRF[i] == 0) {
      if (effFake[i] == 1.f) return 0.f;
      return (1.f / (effFake[i] - 1.f));
    }
    else {
      std::cerr << "Error: unknown TLOut, RF config" << std::endl;
      std::cerr << configToStr(configTLOut) << " " << configToStr(configRF) << std::endl;
      throw;
    }
  }

  float MatrixMethod::dLogInvPhidReal(unsigned int i) const
  {
    float scale = 1.f / (effFake[i] - effReal[i]);
    if (configRF[i] == 1 && configTLIn[i] == 1) {
      return (scale * 1.f);
    }
    else if (configRF[i] == 1 && configTLIn[i] == 0) {
      return (scale * 1.f);
    }
    else if (configRF[i] == 0 && configTLIn[i] == 1) {
      if (effReal[i] == 1.f) return 0.f;
      return (scale * (effFake[i] - 1.f)/(effReal[i] - 1.f));
    }
    else if (configRF[i] == 0 && configTLIn[i] == 0) {
      if (effReal[i] == 0.f) return 0.f;
      return (scale * effFake[i] / effReal[i]);
    }
    else {
      std::cerr << "Error: unknown RF, TLIn config" << std::endl;
      std::cerr << configToStr(configRF) << " " << configToStr(configTLIn) << std::endl;
      throw;
    }
  }

  float MatrixMethod::dLogInvPhidFake(unsigned int i) const
  {
    float scale = 1.f / (effReal[i] - effFake[i]);
    if (configRF[i] == 1 && configTLIn[i] == 1) {
      if (effFake[i] == 1.f) return 0.f;
      return (scale * (effReal[i] - 1.f)/(effFake[i] - 1.f));
    }
    else if (configRF[i] == 1 && configTLIn[i] == 0) {
      if (effFake[i] == 0.f) return 0.f;
      return (scale * effReal[i] / effFake[i]);
    }
    else if (configRF[i] == 0 && configTLIn[i] == 1) {
      return (scale * 1.f);
    }
    else if (configRF[i] == 0 && configTLIn[i] == 0) {
      return (scale * 1.f);
    }
    else {
      std::cerr << "Error: unknown RF, TLin config" << std::endl;
      std::cerr << configToStr(configRF) << " " << configToStr(configTLIn) << std::endl;
      throw;
    }
  }

  void MatrixMethod::addVarianceInformation(MatrixMethod::Result &result) const
  {
    unsigned int numBins = uncsF.size();
    result.varBins.clear(); 
    result.varBins.resize(numBins, 0.f);
    result.varElCorr = 0.f;
    result.varMuCorr = 0.f;

    for (unsigned int bin = 0; bin < numBins; ++bin) {
      float sumOfDerivatives = 0.f;
      for (unsigned int lep = 0; lep < numLeptons; ++lep) {
        if (uEffFakeBinIndex[lep] != bin) continue;
        result.varBins[bin] = uEffFakeStat[lep] * uEffFakeStat[lep] +
                              uEffFakeUncorr[lep] * uEffFakeUncorr[lep];
        sumOfDerivatives += derivativesFake[lep];
      }
      result.varBins[bin] *= sumOfDerivatives * sumOfDerivatives;
    }


    float sumElFakeCorr = 0.f;
    float sumElReal = 0.f;
    float sumMuFakeCorr = 0.f;
    float sumMuReal = 0.f;
    for (unsigned int lep = 0; lep < numLeptons; ++lep) {
      if (isLepElectron[lep]) {
        sumElFakeCorr += derivativesFake[lep] * uEffFakeCorr[lep];
        sumElReal += fabs(derivativesReal[lep]) * uEffReal[lep];
      } else {
        sumMuFakeCorr += derivativesFake[lep] * uEffFakeCorr[lep];
        sumMuReal += fabs(derivativesReal[lep]) * uEffReal[lep];
      }
    }

    result.varElCorr = sumElFakeCorr * sumElFakeCorr + sumElReal * sumElReal;
    result.varMuCorr = sumMuFakeCorr * sumMuFakeCorr + sumMuReal * sumMuReal;
  }

  void MatrixMethod::clearMemory()
  {
    delete [] isLepElectron;
    delete [] effReal;
    delete [] effFake;
    delete [] uEffReal;
    delete [] uEffFakeStat;
    delete [] uEffFakeUncorr;
    delete [] uEffFakeCorr;
    delete [] uEffFakeBinIndex;
    delete [] configTLIn;
    delete [] configTLOut;
    delete [] configRF;
    delete [] derivativesReal;
    delete [] derivativesFake;
  }

  std::string MatrixMethod::configToStr(const char *config) const
  {
    std::string result;
    for (unsigned int i = 0; i < numLeptons; ++i) {
      char lep;
      if (config[i] == 0) {
        lep = '0';
      } else if (config[i] == 1) {
        lep = '1';
      } else {
        lep = config[i];
      }
      result += lep;
    }
    return result;
  }
}
