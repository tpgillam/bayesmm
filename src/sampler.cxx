#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>


#include "ComplexEfficiencyInfo.h"
#include "MatrixMethodApplicator.h"
#include "MMGibbsPosteriorSampler.h"
#include "utils.h"

namespace RunType {
  enum e {
    MATRIX_METHOD, 
    MM_GIBBS_SAMPLER,
    MM_SPARSE_GIBBS_SAMPLER
  };
}

void printMMSamples(const RunType::e& runType,
                    const ComplexEfficiencyInfo& efficiencyInfo,
                    std::istream& stream, unsigned int nSamples,
                    unsigned int nSamplesPerEfficiencyPoint
                    );


unsigned int numLeptonsInStream(std::istream& stream)
{
  std::string line;
  int pos = stream.tellg();
  std::getline(stream, line);
  stream.seekg(pos);

  std::vector<std::string> elems;
  split(line, ' ', elems); 
  if (elems.size() % 2 != 0) {
    throw std::runtime_error("ERROR: odd number of columns per line in event file!");
  }
  unsigned int nLeptons = elems.size() / 2;

  return nLeptons;
}


void printMMSamples(const RunType::e& runType,
                    const ComplexEfficiencyInfo& efficiencyInfo,
                    std::istream& stream, unsigned int nSamples, unsigned int nSamplesPerEfficiencyPoint)
{
  const unsigned int nLeptons = numLeptonsInStream(stream);

  if (runType == RunType::MATRIX_METHOD) {
    MatrixMethodApplicator mm(nLeptons, efficiencyInfo);
    mm.setRealEfficiencyPriors(efficiencyInfo.realEffMeans, efficiencyInfo.realEffUncs);
    mm.setFakeEfficiencyPriors(efficiencyInfo.fakeEffMeans, efficiencyInfo.fakeEffUncs);
    // This prints out the result
    mm.grabObservedEvents(stream);
  } else if (runType == RunType::MM_GIBBS_SAMPLER) {
    MMGibbsPosteriorSampler mmGibbsSampler(nLeptons, efficiencyInfo, nSamplesPerEfficiencyPoint);
    mmGibbsSampler.grabObservedEvents(stream);
    mmGibbsSampler.drawSamples(nSamples);
  } else if (runType == RunType::MM_SPARSE_GIBBS_SAMPLER) {
    MMGibbsPosteriorSampler mmSparseGibbsSampler(nLeptons, efficiencyInfo, nSamplesPerEfficiencyPoint, true);
    mmSparseGibbsSampler.grabObservedEvents(stream);
    mmSparseGibbsSampler.drawSamples(nSamples);
  } else {
    std::cerr << runType << std::endl;
    throw std::runtime_error("Unhandled run type");
  }
}



int main(int argc, char* argv[])
{
  if (argc < 3) {
    std::cerr << "Usage: ./sampler <RunType> <efficiencyInfo> <nSamples> <nSamplesPerEfficiencyPoint>"  << std::endl << std::endl
    << "<RunType>:" << std::endl
    << "  0: MATRIX_METHOD" << std::endl
    << "       print out one line with matrix method estimate and standard uncertainty. The last two arguments are irrelevant and ignored." << std::endl << std::endl
    << "  1: MM_GIBBS_SAMPLER" << std::endl
    << "       print out <nSamples> lines; these are draws from the target probability distribution." << std::endl << std::endl
    << "  2: MM_SPARSE_GIBBS_SAMPLER" << std::endl
    << "       as for MM_GIBBS_SAMPLER, but ignore any event category with no observed events. Almost always a sensible choice." << std::endl << std::endl
    << "<efficiencyInfo>:             filename of efficiency configuration file" << std::endl
    << "<nSamples>:                   the number of samples to print (-1 for infinite, default)" << std::endl
    << "<nSamplesPerEfficiencyPoint>: the number of samples to make per efficiency draw (Gibbs) (default 100)" << std::endl;
    exit(EXIT_FAILURE);
  }
  RunType::e runType = static_cast<RunType::e>(std::stoi(argv[1]));
  std::string efficiencyConfigFilename(argv[2]);
  unsigned int nSamples = -1;
  if (argc > 3) {
    nSamples = std::stoi(argv[3]);
  }

  unsigned int nSamplesPerEfficiencyPoint = 100;
  if (argc > 4) {
    nSamplesPerEfficiencyPoint = std::stoi(argv[4]);
  }
  
  ComplexEfficiencyInfo efficiencyInfo(efficiencyConfigFilename);

  std::ostringstream stream;
  stream << std::cin.rdbuf();
  std::string input = stream.str();
  std::istringstream readStream(input);

  printMMSamples(runType, efficiencyInfo, readStream, nSamples, nSamplesPerEfficiencyPoint);

  return EXIT_SUCCESS;
}
