/**
 * @file MatrixMethod.h
 * @author Thomas Gillam <thomas.gillam@cern.ch>
 * @date May, 2013
 * @brief Estimate fake components for events for arbitrary number of leptons
 *
 **/ 

#ifndef GENERALISEDMATRIXMETHOD_MATRIX_METHOD_H
#define GENERALISEDMATRIXMETHOD_MATRIX_METHOD_H

#include <vector>
#include <string>

namespace GeneralisedMatrixMethod {
  class MatrixMethod {
    public:
      MatrixMethod() : m_computeUncertainty(true), m_presetNumLeptons(false) { }
      MatrixMethod(bool computeUncertainty) : m_computeUncertainty(computeUncertainty), m_presetNumLeptons(false) { }
      MatrixMethod(bool computeUncertainty, int nLeptons);
      ~MatrixMethod();


      inline void setRealEfficiencyPriors(const std::vector<double> &means) {
        meansR = means;
      }

      inline void setFakeEfficiencyPriors(const std::vector<double> &means) {
        meansF = means;
      }


      inline void setRealEfficiencyPriors(const std::vector<double> &means, const std::vector<double> &uncs) {
        meansR = means;
        uncsR = uncs;
      }

      inline void setFakeEfficiencyPriors(const std::vector<double> &means, const std::vector<double> &uncs) {
        meansF = means;
        uncsF = uncs;
      }


      struct Lepton {
        Lepton() : effRealIndex(0), effFakeIndex(0), isTight(false) { }
        Lepton(unsigned int omegaHat, bool isTight)
          : effRealIndex(omegaHat), effFakeIndex(omegaHat), isTight(isTight) { }
        Lepton(unsigned int effRealIndex, unsigned int effFakeIndex, bool isTight)
          : effRealIndex(effRealIndex), effFakeIndex(effFakeIndex), isTight(isTight) { }

        unsigned int effRealIndex, effFakeIndex;
        bool isTight;
      };

      struct Result {
        Result(unsigned int nLeptons, char *configTLOut, float weight) : weight(weight)
        {
          for (unsigned int i = 0; i < nLeptons; ++i) {
            bool flag = (bool)configTLOut[i];
            leptonTightList.push_back(flag);
          }
        }

        std::vector<bool> leptonTightList;
        float weight;
        float varElCorr;
        float varMuCorr;
        std::vector<float> varBins;
      };

      std::vector<Result> weightsForInput(const std::vector<Lepton> &leptons);

    private:
      void setNumberOfLeptons();
      bool haveEnoughLeptons() const;
      void allocateMemory();
      void fillLeptonInformation();
      void zeroConfiguration(char *config);
      Result resultForCurrentTLOutConfiguration();
      void zeroFloatArray(float *array);

      void clearMemory();

      bool isLastConfiguration(const char *config) const;
      void incrementConfiguration(char *config);
      bool isWantedTLOutConfiguration() const;
      bool isWantedRFConfiguration() const;
      bool areConfigsEquivalent(const char *config1, const char *config2) const;
      unsigned int numTightOrRealInConfig(const char *config) const;
      float getMatrixElement() const;
      float getInverseFactor() const;
      void incrementDerivatives(float weight);
      float dLogPhidReal(unsigned int i) const;
      float dLogPhidFake(unsigned int i) const;
      float dLogInvPhidReal(unsigned int i) const;
      float dLogInvPhidFake(unsigned int i) const;
      void addVarianceInformation(Result &result) const;

      std::string configToStr(const char *config) const;

    private:
      bool m_computeUncertainty;
      bool m_presetNumLeptons;
      const std::vector<Lepton>* leptons;
      unsigned int numLeptons;

      std::vector<double> meansR, uncsR, meansF, uncsF;

      bool *isLepElectron;
      float *effReal;
      float *effFake;
      float *uEffReal;
      float *uEffFakeStat;
      float *uEffFakeUncorr;
      float *uEffFakeCorr;
      unsigned int *uEffFakeBinIndex;

      char *configTLIn;
      char *configTLOut;
      char *configRF;

      float *derivativesReal;
      float *derivativesFake;
  };
}

#endif // GENERALISEDMATRIXMETHOD_MATRIX_METHOD_H
