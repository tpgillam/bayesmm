#ifndef TL_SPACE_SOLVER_H
#define TL_SPACE_SOLVER_H

#include "Configs.h"

#include <vector>
#include <glpk.h>

class TLSpaceSolver {
  public:
   TLSpaceSolver(const std::vector<double>* lepRealEffs,
                 const std::vector<double>* lepFakeEffs,
                 const std::vector<TLConfigVector>& tlConfigs,
                 const std::vector<RFConfigVector>& rfConfigs
                 );
   ~TLSpaceSolver();

    void clearConstraints();
    double lowerLimit(unsigned int tlIndex);
    double upperLimit(unsigned int tlIndex);
    void addConstraint(unsigned int tlIndex, double rateTL);
    void removeConstraint(unsigned int tlIndex);


  private:
    void createLP();
    double solveForDirection(unsigned int tlIndex, int direction);


  private:
    const std::vector<double>* m_lepRealEffs;
    const std::vector<double>* m_lepFakeEffs;
    std::vector<TLConfigVector> m_tlConfigs;
    std::vector<RFConfigVector> m_rfConfigs;

    // The number of variables, and the maximum number of constraints
    unsigned int N;
    unsigned int maxNumConstraints;


    // The linear programming object
    glp_prob* m_lp;

    // Index of the constraint in the matrix
    int* m_iConstraintMatrix;

    // Index of the structural variable in the matrix
    int* m_jConstraintMatrix;
    
    // Storage for the constraints
    double* m_constraintMatrix;

    // Storage for the constraint targets
    double* m_constraintTargets;

    // Parameters with which we call the algorithm
    glp_smcp m_simplexParams;
};


#endif // TL_SPACE_SOLVER_H
