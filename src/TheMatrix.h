#ifndef THE_MATRIX_H
#define THE_MATRIX_H

#include "Configs.h"

#include <exception>
#include <vector>

class TheMatrix {
  public:

    // Compute the matrix element involved in the matrix method
    static double matrixElement(
        const RFConfigVector& rfConfig, 
        const TLConfigVector& tlConfig,
        const std::vector<double>& effReals,
        const std::vector<double>& effFakes
        )
    {
      const unsigned int nLeptons = effReals.size();
      double element = 1.;
      using namespace TLConfig;
      using namespace RFConfig;
      for (unsigned int i = 0; i < nLeptons; ++i) {
        // Pick out cofactor
        if (tlConfig[i] == TIGHT && rfConfig[i] == REAL) {
          element *= effReals[i];
        }
        else if (tlConfig[i] == LOOSE && rfConfig[i] == REAL) {
          element *= (1. - effReals[i]);
        }
        else if (tlConfig[i] == TIGHT && rfConfig[i] == FAKE) {
          element *= effFakes[i];
        }
        else if (tlConfig[i] == LOOSE && rfConfig[i] == FAKE) {
          element *= (1. - effFakes[i]);
        } else {
          throw std::runtime_error("Oh bother");
        }
      }
      return element;
    }
    
    // Compute the element of the inverse matrix involved in the matrix method
    static double inverseElement(
        const RFConfigVector& rfConfig, 
        const TLConfigVector& tlConfig,
        const std::vector<double>& effReals,
        const std::vector<double>& effFakes
        )
    {
      const unsigned int nLeptons = effReals.size();
      double element = 1.;
      using namespace TLConfig;
      using namespace RFConfig;
      for (unsigned int i = 0; i < nLeptons; ++i ) {
        // Pick out cofactor
        if (tlConfig[i] == TIGHT && rfConfig[i] == REAL) {
          element *= 1. - effFakes[i];
        }
        else if (tlConfig[i] == LOOSE && rfConfig[i] == REAL) {
          element *= - effFakes[i];
        }
        else if (tlConfig[i] == TIGHT && rfConfig[i] == FAKE) {
          element *= - (1. - effReals[i]);
        }
        else if (tlConfig[i] == LOOSE && rfConfig[i] == FAKE) {
          element *= effReals[i];
        } else {
          throw std::runtime_error("Oh bother");
        }
        
        // Divide by determinant
        element /= effReals[i] - effFakes[i];
      }
      return element;
    }

};

#endif // THE_MATRIX_H
