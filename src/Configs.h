#ifndef CONFIGS_H
#define CONFIGS_H

#include <vector>
#include <string>
#include <sstream>
#include <type_traits>
#include <algorithm>
#include <stdexcept>
#include <ostream>

namespace TLConfig {
  enum e {
    TIGHT, LOOSE, FLAG
  };
}

namespace RFConfig {
  enum e {
    REAL, FAKE, FLAG
  };
}

template <class T> struct ConfigTraits
{
};

template <> struct ConfigTraits<TLConfig::e>
{
  static const TLConfig::e zero = TLConfig::TIGHT;
  static const TLConfig::e one  = TLConfig::LOOSE;
  static const TLConfig::e flag = TLConfig::FLAG;
  static const char zeroChar = 'T';
  static const char oneChar = 'L';
  static const char flagChar = '!';
  static const bool isBinary = true;
};

template <> struct ConfigTraits<RFConfig::e>
{
  static const RFConfig::e zero = RFConfig::REAL;
  static const RFConfig::e one  = RFConfig::FAKE;
  static const RFConfig::e flag = RFConfig::FLAG;
  static const char zeroChar = 'R';
  static const char oneChar = 'F';
  static const char flagChar = '!';
  static const bool isBinary = true;
};

template <> struct ConfigTraits<int>
{
  static const int zero = 0;
  static const int increment  = 1;
  static const int flag = -1;
  static const bool isBinary = false;
};



template <class T>
class ConfigVector {
  public:
    ConfigVector() :
      m_limit(ConfigTraits<T>::zero)
    {
    }

    template <class T1 = T>
    ConfigVector(typename std::enable_if<ConfigTraits<T1>::isBinary, unsigned int>::type nElements) :
      m_limit(ConfigTraits<T1>::zero)
    {
      m_configVector.resize(nElements, (T1) ConfigTraits<T1>::zero);
    }

    template <class U, class T1 = T>
    ConfigVector(typename std::enable_if<!ConfigTraits<T1>::isBinary, unsigned int>::type nElements, U limit)
    {
      m_configVector.resize(nElements, (T1) ConfigTraits<T1>::zero);
      _setLimit(limit);
    }

    void reset() {
      std::fill(m_configVector.begin(), m_configVector.end(), (T) ConfigTraits<T>::zero);
    }

    bool isLast()
    {
      for (unsigned int i = 0; i < m_configVector.size(); ++i) {
        if (m_configVector[i] == ConfigTraits<T>::flag) return true;
      }
      return false;
    }
    
    void increment() { _increment(); }
    std::string str() const { return _str(); }

    inline T &operator[] (unsigned int index) {
      return m_configVector[index];
    }

    inline const T &operator[] (unsigned int index) const {
      return m_configVector[index];
    }

    bool operator==(const ConfigVector<T>& other) const {
      if (m_configVector.size() != other.m_configVector.size()) return false;
      if (m_limit != other.m_limit) return false;

      for (unsigned int i = 0; i < m_configVector.size(); ++i) {
        if ((*this)[i] != other[i]) return false;
      }

      return true;
    }

    template <typename U>
    friend std::ostream& operator<<(std::ostream& os, const ConfigVector<U>& configVector);


  private:
    template <class U, class T1 = T>
    typename std::enable_if<ConfigTraits<T1>::isBinary, void>::type
    _setLimit(U, T1 *t1 = 0) {
      throw std::runtime_error("Cannot set limit on binary configuration");
    }

    template <class U, class T1 = T>
    typename std::enable_if<!ConfigTraits<T1>::isBinary, void>::type
    _setLimit(U limit, T1 *t1 = 0) {
      m_limit = (T1) limit;
    }

    void flagAsFinished() {
      if (m_configVector.size() > 0) m_configVector[0] = ConfigTraits<T>::flag;
    }

    template <class T1 = T>
    typename std::enable_if<ConfigTraits<T1>::isBinary, void>::type
    _increment(T1* t1 = 0)
    {
      for (unsigned int i = 0; i < m_configVector.size(); ++i) {
        if (m_configVector[i] == ConfigTraits<T>::zero) {
          m_configVector[i] = ConfigTraits<T>::one;
          return;
        } else {
          m_configVector[i] = ConfigTraits<T>::zero;
        }
      }
      flagAsFinished();
    }

    template <class T1 = T>
    typename std::enable_if<!ConfigTraits<T1>::isBinary, void>::type
    _increment(T1* t1 = 0)
    {
      for (unsigned int i = 0; i < m_configVector.size(); ++i) {
        if (m_configVector[i] != (T) (m_limit - 1)) {
          m_configVector[i] += ConfigTraits<T>::increment;
          return;
        } else {
          m_configVector[i] = ConfigTraits<T>::zero;
        }
      }
      flagAsFinished();
    }

    template <class T1 = T>
    typename std::enable_if<ConfigTraits<T1>::isBinary, std::string>::type
    _str(T1* t1 = 0) const
    {
      std::string result;
      for (unsigned int i = 0; i < m_configVector.size(); ++i) {
        char lep;
        if (m_configVector[i] == ConfigTraits<T>::zero) {
          lep = ConfigTraits<T>::zeroChar;
        } else if (m_configVector[i] == ConfigTraits<T>::one) {
          lep = ConfigTraits<T>::oneChar;
        } else {
          lep = m_configVector[i];
        }
        result += lep;
      }
      return result;
    }

    template <class T1 = T>
    typename std::enable_if<!ConfigTraits<T1>::isBinary, std::string>::type
    _str(T1* t1 = 0) const
    {
      std::ostringstream stream;
      stream << "{";
      for (unsigned int i = 0; i < m_configVector.size(); ++i) {
        if (i == (m_configVector.size() -1)) {
          stream << m_configVector[i];
        } else {
          stream << m_configVector[i] << ", ";
        }
      }
      stream << "}";
      return stream.str();
    }


  private:
    std::vector<T> m_configVector;
    T m_limit;
};


template <typename T>
std::ostream& operator<<(std::ostream& os, const ConfigVector<T>& configVector) {
  os << configVector.str();
  return os;
}


typedef ConfigVector<TLConfig::e> TLConfigVector;
typedef ConfigVector<RFConfig::e> RFConfigVector;
typedef ConfigVector<int>         OHConfigVector;


#endif // CONFIGS_H
