#include "TLSpaceSolver.h"

#include "TheMatrix.h"
#include "utils.h"

#include <exception>
#include <iostream>

TLSpaceSolver::TLSpaceSolver(const std::vector<double>* lepRealEffs,
                             const std::vector<double>* lepFakeEffs,
                             const std::vector<TLConfigVector>& tlConfigs,
                             const std::vector<RFConfigVector>& rfConfigs)
    : m_lepRealEffs(lepRealEffs),
      m_lepFakeEffs(lepFakeEffs),
      m_tlConfigs(tlConfigs),
      m_rfConfigs(rfConfigs),
      m_lp(0),
      m_iConstraintMatrix(0),
      m_jConstraintMatrix(0),
      m_constraintMatrix(0)
{
  // Let N be the number of cateogries, there are potentially
  // that many constraints too
  N = m_tlConfigs.size();
  maxNumConstraints = N;

  // Allocate memory for constraint matrix
  size_t maxSizeOfMatrix = 1 + N*maxNumConstraints;
  m_iConstraintMatrix = new int [maxSizeOfMatrix];
  m_jConstraintMatrix = new int [maxSizeOfMatrix];
  m_constraintMatrix = new double [maxSizeOfMatrix];
  m_constraintTargets = new double [N+1];

  // Initialise indices sensibly
  unsigned int m = 1;
  for (unsigned int i = 1; i <= maxNumConstraints; ++i) {
    for (unsigned int j = 1; j <= N; ++j) {
      m_iConstraintMatrix[m] = i;
      m_jConstraintMatrix[m] = j;
      ++m;
    }
  }

  // Ensure matrix initialised to zero
  clearConstraints();

  // Initialise the simplex algorithm parameters
  glp_init_smcp(&m_simplexParams);
  m_simplexParams.msg_lev = GLP_MSG_OFF;
}


void TLSpaceSolver::createLP()
{
  if (m_lp) {
    glp_delete_prob(m_lp);
  }

  m_lp = glp_create_prob();

  // We have as many as maxNumConstraints equality constraints
  glp_add_rows(m_lp, maxNumConstraints);

  // We have N variables, each of which is greater than 0
  glp_add_cols(m_lp, N);
  // Remember that glpk uses index base of 1!
  for (unsigned int i = 1; i <= N; ++i) {
    glp_set_col_bnds(m_lp, i, GLP_LO, 0., 0.);
  }
}


TLSpaceSolver::~TLSpaceSolver()
{
  if (m_lp) {
    glp_delete_prob(m_lp);
    m_lp = 0;
  }

  if (m_iConstraintMatrix) {
    delete [] m_iConstraintMatrix;
  }

  if (m_jConstraintMatrix) {
    delete [] m_jConstraintMatrix;
  }

  if (m_constraintMatrix) {
    delete [] m_constraintMatrix;
  }

  if (m_constraintTargets) {
    delete [] m_constraintTargets;
  }
}


void TLSpaceSolver::clearConstraints()
{
  // Clear constraint matrix
  for (unsigned int i = 0; i < 1 + N*maxNumConstraints; ++i) {
    m_constraintMatrix[i] = 0.;
  }
  
  // Clear constraint targets
  for (unsigned int i = 0; i <= maxNumConstraints; ++i) {
    m_constraintTargets[i] = 0.;
  }
}


double TLSpaceSolver::lowerLimit(unsigned int tlIndex)
{
  return solveForDirection(tlIndex, GLP_MIN);
}


double TLSpaceSolver::upperLimit(unsigned int tlIndex)
{
  return solveForDirection(tlIndex, GLP_MAX);
}


double TLSpaceSolver::solveForDirection(unsigned int tlIndex, int direction)
{
  // Refresh the linear programming problem - it seems as though
  // they cannot be reliably re-used
  createLP();

  // Load up constraints into problem
  for (unsigned int i = 1; i <= maxNumConstraints; ++i) {
    glp_set_row_bnds(m_lp, i, GLP_FX, m_constraintTargets[i], m_constraintTargets[i]);
  }

  // Load the constraint matrix into the problem
  glp_load_matrix(m_lp, maxNumConstraints * N, m_iConstraintMatrix,
                  m_jConstraintMatrix, m_constraintMatrix);

  // Specify the objective function
  const auto& tlConfig = m_tlConfigs[tlIndex];
  unsigned int j = 1;
  for (const auto& rfConfig : m_rfConfigs) {
    double matrixElement = TheMatrix::matrixElement(
        rfConfig, tlConfig, *m_lepRealEffs, *m_lepFakeEffs);
    glp_set_obj_coef(m_lp, j, matrixElement);
    ++j;
  }

  // Specify the direction to solve
  glp_set_obj_dir(m_lp, direction);

  // Solve!
  glp_simplex(m_lp, &m_simplexParams);
  return glp_get_obj_val(m_lp);
}


void TLSpaceSolver::addConstraint(unsigned int tlIndex, double rateTL)
{
  // Constraint index we're adding
  const unsigned int i = tlIndex + 1;
  if (i > maxNumConstraints) {
    throw std::runtime_error("tlIndex is too large - don't add this constraint!");
  }

  // Set the target of the constraint
  m_constraintTargets[i] = rateTL;

  // Set the matrix coefficients according to the efficiencies
  // Start at the correct row
  unsigned int m = 1 + N * tlIndex;
  const auto& tlConfig = m_tlConfigs[tlIndex];
  for (const auto& rfConfig : m_rfConfigs) {
    m_constraintMatrix[m] = TheMatrix::matrixElement(
        rfConfig, tlConfig, *m_lepRealEffs, *m_lepFakeEffs);
    ++m;
  }
}


void TLSpaceSolver::removeConstraint(unsigned int tlIndex)
{
  // Start at the correct row of the matrix
  const unsigned int offset = 1 + N * tlIndex;

  for (unsigned int i = 0; i < N; ++i) { 
    unsigned int m = i + offset;
    m_constraintMatrix[m] = 0.;
  }

  const unsigned int i = tlIndex + 1;

  m_constraintTargets[i] = 0.;
}

