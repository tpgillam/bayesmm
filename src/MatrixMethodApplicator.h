#ifndef MATRIX_METHOD_APPLICATOR_H
#define MATRIX_METHOD_APPLICATOR_H

#include "MatrixMethod.h"
#include "ComplexEfficiencyInfo.h"

#include <iostream>

class MatrixMethodApplicator {
  public:
    MatrixMethodApplicator(unsigned int nLeptons);
    MatrixMethodApplicator(unsigned int nLeptons, const ComplexEfficiencyInfo& efficiencyInfo);

    void setRealEfficiencyPriors(const std::vector<double> &means, const std::vector<double> &uncs);
    void setFakeEfficiencyPriors(const std::vector<double> &means, const std::vector<double> &uncs);

    void grabObservedEvents(std::istream &stream = std::cin);

    inline double predictedWeight() const { return m_predictedWeight; }
    inline double predictedUncertainty() const { return m_predictedUncertainty; }
    inline unsigned int nObsTot() const { return m_nObsTot; }

  private:
    unsigned int m_nLeptons;
    unsigned int m_nObsTot;
    GeneralisedMatrixMethod::MatrixMethod m_matrixMethod;

    double m_predictedWeight;
    double m_predictedUncertainty;

    const ComplexEfficiencyInfo* m_efficiencyInfo;
};

#endif // MATRIX_METHOD_APPLICATOR_H
