#include "utils.h"

#include <boost/algorithm/string.hpp>

#include <cstdlib>

double randUniform(double maxValue)
{
  return maxValue*((double) rand() / (RAND_MAX));
}

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems)
{
  elems.clear();
  boost::split(elems, s, boost::is_any_of(" \t"), boost::token_compress_on);
  return elems;
}
